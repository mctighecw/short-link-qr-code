package main

import (
	"fmt"

	"short-link-qr-code-backend/internal/config"
	"short-link-qr-code-backend/internal/db"

	"go.mongodb.org/mongo-driver/bson"
)

var (
	DbName    = config.DB_NAME
	CollLinks = "links"
)

func main() {
	if !config.IS_PRODUCTION {
		client, ctx := db.DbConnect()
		db := client.Database(DbName)
		coll := db.Collection(CollLinks)

		f := bson.M{}
		_, err := coll.DeleteMany(ctx, f)

		if err != nil {
			m := fmt.Sprintf("Error dropping database %s", DbName)
			fmt.Println(m)
		} else {
			fmt.Println("Database has been dropped")
		}
	} else {
		fmt.Println("Can only drop database in development environment")
	}
}
