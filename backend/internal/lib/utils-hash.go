package lib

import (
	"crypto/rand"
	"math/big"
)

// Exclude ambiguous characters such as O, 0, 1, l, I
const (
	upperLetters = "ABCDEFGHJKLMNPQRSTUVWXYZ"
	lowerLetters = "abcdefghijkmnopqrstuvwxyz"
	numbers      = "23456789"
)

var (
	allChars    = upperLetters + lowerLetters + numbers
	allCharsLen = len(allChars)
)

func MakeCleanRandomString(length int) string {
	// Generate a random string of the specified length.
	// The string will contain at least one uppercase letter, one lowercase letter and no more than two numbers.

	for {
		hash := make([]byte, length)
		upperCount, lowerCount, numberCount := 0, 0, 0

		for i := 0; i < length; i++ {
			index, _ := rand.Int(rand.Reader, big.NewInt(int64(allCharsLen)))

			char := allChars[index.Int64()]
			hash[i] = char

			switch {
			case char >= 'A' && char <= 'Z':
				upperCount++
			case char >= 'a' && char <= 'z':
				lowerCount++
			case char >= '2' && char <= '9':
				numberCount++
			}
		}

		if upperCount > 0 && lowerCount > 0 && numberCount <= 2 {
			return string(hash)
		}
	}
}
