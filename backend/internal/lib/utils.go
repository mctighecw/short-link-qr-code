package lib

import (
	"fmt"
	"net/url"
	"os"
	"strings"

	"github.com/labstack/echo/v4"
)

func LogError(err error) {
	if err != nil {
		fmt.Println(err)
		return
	}
}

func ErrorMessage(message string) interface{} {
	res := map[string]interface{}{
		"status":  "Error",
		"message": message,
	}
	return res
}

func GetEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func GetEchoMap(c echo.Context) echo.Map {
	m := echo.Map{}
	if err := c.Bind(&m); err != nil {
		LogError(err)
	}
	return m
}

func ReturnStringValue(m echo.Map, field string) string {
	v := fmt.Sprintf("%v", m[field])
	return v
}

func If[T any](cond bool, vTrue, vFalse T) T {
	if cond {
		return vTrue
	}
	return vFalse
}

func GetHeader(c echo.Context, key string) string {
	return c.Request().Header.Get(key)
}

func ProcessURL(rawURL string) (string, error) {
	// Process the URL: remove the scheme and make the host lowercase

	// parse the URL
	parsedURL, err := url.Parse(rawURL)
	if err != nil {
		return "", err
	}

	host := parsedURL.Host
	pathAndQuery := parsedURL.RequestURI()

	// make the host lowercase
	lowercaseHost := strings.ToLower(host)

	// construct the new URL without the scheme
	newURL := lowercaseHost + pathAndQuery

	return newURL, nil
}
