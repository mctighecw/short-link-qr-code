package lib

func ReturnError(m string) interface{} {
	r := map[string]interface{}{"status": "Error", "message": m}
	return r
}

func ReturnOK(m string) interface{} {
	r := map[string]interface{}{"status": "OK", "message": m}
	return r
}

func ReturnData(m string, d interface{}) interface{} {
	r := map[string]interface{}{"status": "OK", "message": m, "data": d}
	return r
}
