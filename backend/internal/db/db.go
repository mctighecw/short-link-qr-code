package db

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"short-link-qr-code-backend/internal/config"
	"short-link-qr-code-backend/internal/lib"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func DbConnect() (*mongo.Client, context.Context) {
	timeout := 10 * time.Second
	port := strconv.FormatInt(int64(config.DB_CONFIG.Port), 10)
	uri := fmt.Sprintf("mongodb://%s:%s@%s:%s/?authSource=%s",
		config.DB_CONFIG.User,
		config.DB_CONFIG.Password,
		config.DB_CONFIG.Host,
		port,
		config.DB_CONFIG.AdminDb,
	)

	ctx, _ := context.WithTimeout(context.Background(), timeout)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	lib.LogError(err)

	return client, ctx
}
