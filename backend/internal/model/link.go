package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Link struct {
	ID             primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	OriginalLink   string             `bson:"original_link" json:"original_link"`
	LinkHash       string             `bson:"link_hash" json:"link_hash"`
	Count          int                `bson:"count" json:"count"`
	CreatedAt      time.Time          `bson:"created_at" json:"created_at,omitempty"`
	LastAccessedAt time.Time          `bson:"last_accessed_at" json:"last_accessed_at,omitempty"`
}
