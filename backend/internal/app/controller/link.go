package controller

import (
	"errors"
	"fmt"
	"time"

	"short-link-qr-code-backend/internal/config"
	"short-link-qr-code-backend/internal/db"
	"short-link-qr-code-backend/internal/lib"
	"short-link-qr-code-backend/internal/model"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	CollLinks = "links"
)

var (
	DbName = config.DB_NAME
)

func GenerateLinkHash() string {
	linkHash := lib.MakeCleanRandomString(5)
	return linkHash
}

func getLinkUrl(l string) string {
	return fmt.Sprintf("%s/%s", config.SHORT_LINK_BASE_URL, l)
}

func ReturnLinkData(u model.Link) interface{} {
	shortLink := getLinkUrl(u.LinkHash)

	res := map[string]interface{}{
		"original_link": u.OriginalLink,
		"short_link":    shortLink,
	}
	return res
}

func ReturnDetailedLinkData(u model.Link) interface{} {
	shortLink := getLinkUrl(u.LinkHash)

	res := map[string]interface{}{
		"id":               u.ID.Hex(),
		"original_link":    u.OriginalLink,
		"short_link":       shortLink,
		"link_hash":        u.LinkHash,
		"count":            u.Count,
		"created_at":       u.CreatedAt,
		"last_accessed_at": u.LastAccessedAt,
	}
	return res
}

func ReturnAllLinkData(d []model.Link) interface{} {
	e := []interface{}{}

	for _, el := range d {
		f := ReturnDetailedLinkData(el)
		e = append(e, f)
	}

	res := lib.ReturnData("Link data retrieved successfully", e)
	return res
}

func SaveNewLink(originalLink string, linkHash string) (model.Link, error) {
	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollLinks)

	var u model.Link

	// Check if original link already exists
	findRes1 := coll.FindOne(ctx, bson.M{"original_link": originalLink})
	err := findRes1.Decode(&u)

	// Return existing link data
	if err == nil {
		return u, err
	}

	// TODO: improve this check
	findRes2 := coll.FindOne(ctx, bson.M{"link_hash": linkHash})
	err2 := findRes2.Err()

	if err2 == nil {
		err := errors.New("error: link hash already exists")
		return u, err
	}

	u = model.Link{
		ID:           primitive.NewObjectID(),
		OriginalLink: originalLink,
		LinkHash:     linkHash,
		Count:        0,
		CreatedAt:    time.Now(),
	}

	_, err3 := coll.InsertOne(ctx, u)
	lib.LogError(err3)

	if err3 != nil {
		return u, err3
	}

	return u, nil
}

func UpdateLinkAccess(id primitive.ObjectID) error {
	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollLinks)

	_, err := coll.UpdateOne(ctx,
		bson.M{"_id": id}, bson.M{
			"$inc": bson.M{"count": 1},
			"$set": bson.M{"last_accessed_at": time.Now()},
		},
	)
	if err != nil {
		return err
	}

	return nil
}

func GetLink(linkHash string) (model.Link, error) {
	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollLinks)

	var u model.Link
	findRes := coll.FindOne(ctx, bson.M{"link_hash": linkHash})
	err := findRes.Decode(&u)

	if err != nil {
		return u, err
	}

	return u, nil
}

func GetLinks() ([]model.Link, error) {
	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollLinks)

	var u model.Link
	var res []model.Link
	cursor, err := coll.Find(ctx, bson.M{})
	if err != nil {
		return res, err
	}

	for cursor.Next(ctx) {
		cursor.Decode(&u)
		res = append(res, u)
	}

	return res, nil
}

func DeleteLink(id string) error {
	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollLinks)

	objID, _ := primitive.ObjectIDFromHex(id)
	_, err := coll.DeleteOne(ctx, bson.M{"_id": objID})
	if err != nil {
		return err
	}

	return nil
}
