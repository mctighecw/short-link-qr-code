package handler

import (
	"net/http"

	"short-link-qr-code-backend/internal/app/controller"
	"short-link-qr-code-backend/internal/config"
	"short-link-qr-code-backend/internal/lib"

	"github.com/labstack/echo/v4"
)

// @Summary      Create new link
// @Description  Post a long URL link and create a new, short URL link.
// @Tags         link
// @Produce      json
// @Success      200 {object} lib.DataResponse
// @Failure      500 {object} lib.StatusResponse
// @Router       /link/create [POST]
func CreateLink(c echo.Context) error {
	m := lib.GetEchoMap(c)

	originalLinkRaw := lib.ReturnStringValue(m, "original_link")

	originalLink, err1 := lib.ProcessURL(originalLinkRaw)
	if err1 != nil {
		res := lib.ReturnError("Error processing URL")
		return c.JSON(http.StatusInternalServerError, res)
	}

	linkHash := controller.GenerateLinkHash()

	u, err2 := controller.SaveNewLink(originalLink, linkHash)
	if err2 != nil {
		res := lib.ReturnError("Error saving link")
		return c.JSON(http.StatusInternalServerError, res)
	}

	d := controller.ReturnLinkData(u)
	res := lib.ReturnData("Short link created successfully", d)

	return c.JSON(http.StatusOK, res)
}

// @Summary      Delete existing link
// @Description  Hard delete a link.
// @Tags         link protected
// @Produce      json
// @Param        id path string true "id"
// @Success      200 {object} lib.StatusResponse
// @Failure      401 {object} lib.StatusResponse
// @Failure      404 {object} lib.StatusResponse
// @Router       /link/:id [DELETE]
func DeleteLink(c echo.Context) error {
	// Check authorization
	accessKey := lib.GetHeader(c, "Admin-Access-Key")

	if accessKey != config.ADMIN_ACCESS_KEY {
		res := lib.ReturnError("Authorization is required")
		return c.JSON(http.StatusUnauthorized, res)
	}

	id := c.Param("id")

	err := controller.DeleteLink(id)
	if err != nil {
		res := lib.ReturnError("Link not found")
		return c.JSON(http.StatusNotFound, res)
	}

	res := lib.ReturnOK("Link deleted successfully")
	return c.JSON(http.StatusOK, res)
}

// @Summary      Get existing link
// @Description  Return public information for link.
// @Tags         link
// @Produce      json
// @Param        id path string true "link hash"
// @Success      200 {object} lib.DataResponse
// @Failure      400 {object} lib.StatusResponse
// @Failure      404 {object} lib.StatusResponse
// @Router       /link/:id [GET]
func GetLink(c echo.Context) error {
	linkHash := c.Param("id")

	if len(linkHash) != 5 {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}

	u, err := controller.GetLink(linkHash)
	if err != nil {
		res := lib.ReturnError("Link not found")
		return c.JSON(http.StatusNotFound, res)
	}

	d := controller.ReturnLinkData(u)
	res := lib.ReturnData("Link data retrieved successfully", d)

	return c.JSON(http.StatusOK, res)
}

// @Summary      Get all existing links
// @Description  Return all private information for all links.
// @Tags         link protected
// @Produce      json
// @Success      200 {object} lib.DataResponse
// @Failure      401 {object} lib.StatusResponse
// @Failure      500 {object} lib.StatusResponse
// @Router       /link [GET]
func GetLinks(c echo.Context) error {
	// Check authorization
	accessKey := lib.GetHeader(c, "Admin-Access-Key")

	if accessKey != config.ADMIN_ACCESS_KEY {
		res := lib.ReturnError("Authorization is required")
		return c.JSON(http.StatusUnauthorized, res)
	}

	res, err := controller.GetLinks()
	if err != nil {
		res := lib.ReturnError("An error has occurred")
		return c.JSON(http.StatusInternalServerError, res)
	}

	d := controller.ReturnAllLinkData(res)
	return c.JSON(http.StatusOK, d)
}

// @Summary      Redirect link
// @Description  Redirect from link hash to original link.
// @Tags         link
// @Param        id path string true "link hash"
// @Produce      json
// @Success      303
// @Failure      400 {object} lib.StatusResponse
// @Failure      404 {object} lib.StatusResponse
// @Router       /link/:id [GET]
func LinkRedirect(c echo.Context) error {
	linkHash := c.Param("id")

	if len(linkHash) != 5 {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}

	u, err := controller.GetLink(linkHash)
	if err != nil {
		return c.JSON(http.StatusNotFound, "Link not found")
	}

	// Update link count and last access timestamp
	controller.UpdateLinkAccess(u.ID)

	redirectLink := "https://" + u.OriginalLink

	return c.Redirect(http.StatusSeeOther, redirectLink)
}
