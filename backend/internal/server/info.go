package server

// General API Info for Swagger
// @title            Short Link QR Code Backend
// @version          1.0
// @description      This is a web server using the Go Echo framework.
// @contact.email    mctighecw@gmail.com
// @license.name     MIT
// @host             localhost:8000
// @BasePath         /api
// @Schemes          http https
