package server

import (
	"net/http"
	"time"

	"short-link-qr-code-backend/internal/lib"
	"short-link-qr-code-backend/internal/logger"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"go.uber.org/zap"
)

func addMiddleware(e *echo.Echo) {
	// Env
	APP_ENV := lib.GetEnv("APP_ENV", "development")
	allowedOrigins := lib.GetEnv("ALLOWED_ORIGINS", "")
	headers := []string{}

	// All Middleware
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{allowedOrigins},
		AllowMethods:     []string{http.MethodHead, http.MethodOptions, http.MethodGet, http.MethodPost, http.MethodPut, http.MethodDelete},
		AllowCredentials: true,
		AllowHeaders:     headers,
		ExposeHeaders:    headers,
		MaxAge:           3600,
	}))

	if APP_ENV == "production" {
		e.Use(middleware.SecureWithConfig(middleware.SecureConfig{
			XSSProtection:         "1; mode=block",
			ContentTypeNosniff:    "nosniff",
			XFrameOptions:         "SAMEORIGIN",
			ContentSecurityPolicy: "default-src 'self'",
		}))
	}

	e.Use(middleware.TimeoutWithConfig(middleware.TimeoutConfig{
		ErrorMessage: "Request has exceeded 20 seconds and timed out",
		Timeout:      20 * time.Second,
	}))

	// Rate limit 20 requests per second
	e.Use(middleware.RateLimiterWithConfig(middleware.RateLimiterConfig{
		Store: middleware.NewRateLimiterMemoryStoreWithConfig(
			middleware.RateLimiterMemoryStoreConfig{Rate: 20, ExpiresIn: 1 * time.Second},
		),
		DenyHandler: func(context echo.Context, identifier string, err error) error {
			return &echo.HTTPError{
				Code:     http.StatusTooManyRequests,
				Message:  "Rate limit has been exceeded, too many requests",
				Internal: err,
			}
		},
	}))

	// Limit body size to 1MB
	e.Use(middleware.BodyLimit("1M"))

	e.Use(middleware.Recover())

	e.Use(middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
		LogHost:     true,
		LogLatency:  true,
		LogMethod:   true,
		LogProtocol: true,
		LogStatus:   true,
		LogURI:      true,
		LogValuesFunc: func(c echo.Context, v middleware.RequestLoggerValues) error {
			l := logger.CreateLogger()

			defer l.Sync()

			l.Info("request",
				zap.String("host", v.Host),
				zap.Int64("latency", v.Latency.Milliseconds()),
				zap.String("method", v.Method),
				zap.String("protocol", v.Protocol),
				zap.Int("status", v.Status),
				zap.String("uri", v.URI),
			)

			return nil
		},
	}))
}
