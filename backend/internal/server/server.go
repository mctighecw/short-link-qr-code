package server

import (
	"fmt"

	"short-link-qr-code-backend/internal/config"
	"short-link-qr-code-backend/internal/lib"

	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

func CreateServer() *echo.Echo {
	e := echo.New()

	// Log level
	logLevel := lib.If(config.IS_PRODUCTION, log.ERROR, log.DEBUG)
	e.Logger.SetLevel(logLevel)

	// Middleware
	addMiddleware(e)

	// Routes
	createRoutes(e)

	// Mode
	mode := fmt.Sprintf("Mode: %s", config.APP_ENV)
	fmt.Println(mode)

	return e
}
