package server

import (
	"net/http"

	_ "short-link-qr-code-backend/docs"
	"short-link-qr-code-backend/internal/app/handler"
	"short-link-qr-code-backend/internal/lib"

	"github.com/labstack/echo/v4"
	"github.com/swaggo/echo-swagger"
)

func createRoutes(e *echo.Echo) {
	// Env
	APP_ENV := lib.GetEnv("APP_ENV", "development")

	// Create all routes
	e.GET("/", func(c echo.Context) error {
		errMsg := lib.ErrorMessage("Invalid path")
		return c.JSON(http.StatusNotFound, errMsg)
	})

	// Swagger
	if APP_ENV == "development" {
		e.GET("/swagger/*", echoSwagger.WrapHandler)
	}

	// All API routes
	a := e.Group("/api")

	a.POST("/link/create", handler.CreateLink)
	a.DELETE("/link/:id", handler.DeleteLink)
	a.GET("/link/:id", handler.GetLink)
	a.GET("/link", handler.GetLinks)
	a.GET("/link/redirect/:id", handler.LinkRedirect)
}
