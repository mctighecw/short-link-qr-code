server {
  listen 80 default_server;
  server_name _;

  server_tokens off;

  location / {  
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Origin $http_origin;
    proxy_redirect off;
    proxy_buffering off;
    proxy_request_buffering off;
    proxy_http_version 1.1;
    proxy_intercept_errors on;

    proxy_connect_timeout 30s;
    proxy_send_timeout 30s;
    proxy_read_timeout 30s;

    # Pass path/param to proxied server
    proxy_pass http://short-link-qr-code-backend:8000/api/link/redirect/;
  }

  error_page 400 /400.html;
  location = /400.html {
    root /usr/share/nginx/html;
    index index.html index.htm;
  }

  error_page 404 /404.html;
  location = /404.html {
    root /usr/share/nginx/html;
    index index.html index.htm;
  }

  error_page 500 502 503 504 /50x.html;
  location = /50x.html {
    root /usr/share/nginx/html;
    index index.html index.htm;
  }

  location = /favicon.png {
    alias /usr/share/nginx/html/favicon.png;
  }

  location = /styles.css {
    alias /usr/share/nginx/html/styles.css;
  }
}
