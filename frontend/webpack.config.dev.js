const webpack = require("webpack");
const { merge } = require("webpack-merge");

const common = require("./webpack.common");

module.exports = merge(common, {
  mode: "development",
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(gif|ico|jpe?g|pdf|png|svg)$/,
        use: "file-loader?name=assets/[name].[ext]",
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("development"),
        ADMIN_ACCESS_KEY: JSON.stringify(process.env.ADMIN_ACCESS_KEY),
      },
    }),
  ],
  devtool: "eval-cheap-source-map",
  devServer: {
    port: 3000,
    historyApiFallback: true,
    // To avoid CORS issues
    proxy: [
      {
        context: ["/api"],
        target: "http://host.docker.internal:8000",
      },
    ],
  },
});
