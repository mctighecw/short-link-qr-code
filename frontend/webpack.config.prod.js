const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const { merge } = require("webpack-merge");

const common = require("./webpack.common");

module.exports = merge(common, {
  mode: "production",
  output: {
    filename: "[contenthash].js",
    chunkFilename: "[contenthash].js",
    path: path.resolve(__dirname, "build"),
    publicPath: "/",
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.(gif|ico|jpe?g|pdf|png|svg)$/,
        use: "file-loader?name=assets/[contenthash:10].[ext]",
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production"),
        ADMIN_ACCESS_KEY: JSON.stringify(process.env.ADMIN_ACCESS_KEY),
      },
    }),
    new MiniCssExtractPlugin({
      filename: "[contenthash].css",
      chunkFilename: "[contenthash].css",
    }),
  ],
  devtool: false,
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({ parallel: true }), new CssMinimizerPlugin()],
    moduleIds: "deterministic",
    chunkIds: "deterministic",
    splitChunks: {
      minSize: 17000,
      minRemainingSize: 0,
      minChunks: 1,
      maxAsyncRequests: 30,
      maxInitialRequests: 30,
      automaticNameDelimiter: "_",
      enforceSizeThreshold: 30000,
      cacheGroups: {
        defaultVendors: false,
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
        commons: {
          test: /[\\/]node_modules[\\/]/,
          priority: -5,
          reuseExistingChunk: true,
          chunks: "initial",
          name: "commons",
        },
        vendorReact: {
          test: /[\\/]node_modules[\\/](react?[(?:-\S)]+)[\\/]/,
          name: "vendor_react",
          chunks: "all",
        },
        vendorMui: {
          test: /[\\/]node_modules[\\/](@mui?[(?://\S)]+)[\\/]/,
          name: "vendor_mui",
          chunks: "all",
        },
      },
    },
  },
});
