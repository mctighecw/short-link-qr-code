import { DateTime } from "luxon";

export const convertDate = (dateString) => {
  if (dateString) {
    const dt = DateTime.fromISO(dateString);
    return dt.toLocaleString(DateTime.DATETIME_SHORT);
  }
  return "NA";
};
