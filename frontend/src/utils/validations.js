export const validateUrl = (url) => {
  // validate a URL with or without the http scheme
  const pattern = /^(https?:\/\/)?([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,}(\/[^\s]*)?$/;
  return pattern.test(url);
};
