const getTheme = () => {
  return localStorage.getItem("theme");
};

export const setTheme = (theme) => {
  localStorage.setItem("theme", theme);
};

export const checkAndSetTheme = () => {
  let theme = getTheme();

  if (!theme) {
    theme = matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
    setTheme(theme);
  }

  return theme;
};
