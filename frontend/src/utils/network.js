import axios from "axios";
import applyCaseMiddleware from "axios-case-converter";

// Custom config
const dev = process.env.NODE_ENV === "development";
const timeout = dev ? 30000 : 12000;

// Default settings
axios.defaults.baseURL = "/api";
axios.defaults.withCredentials = true;

// Axios instance
const instance = axios.create();

instance.interceptors.request.use(
  (config) => {
    // Do something before request is sent
    return config;
  },
  (error) => {
    // Do something with request error
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  (response) => {
    // Do something with response data
    return response;
  },
  (error) => {
    // Do something with response error
    return Promise.reject(error);
  }
);

const instanceWithMiddleware = applyCaseMiddleware(instance);

const request = (requestConfig) =>
  instanceWithMiddleware.request({
    timeout,
    ...requestConfig,
  });

export default request;
