import React from "react";
import { Navigate, Route, Routes as RouterRoutes } from "react-router-dom";
import Admin from "@/components/admin";
import Main from "@/components/main";

const Routes = () => (
  <RouterRoutes>
    <Route path="/" element={<Main />} />
    <Route path="/admin" element={<Admin />} />
    <Route path="*" element={<Navigate to="/" replace />} />
  </RouterRoutes>
);

export default Routes;
