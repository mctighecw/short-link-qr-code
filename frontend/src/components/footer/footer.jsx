import React from "react";
import useTheme from "@/hooks/use-theme";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import useMediaQuery from "@mui/material/useMediaQuery";

const Footer = () => {
  const { theme } = useTheme();
  const isMobile = useMediaQuery("(max-width:500px)");
  const bgColor = theme === "light" ? "primary.main" : "dark";

  return (
    <Box bgcolor={bgColor} sx={{ p: 1 }}>
      <Typography variant="h6" sx={{ color: "white" }}>
        © 2024 Christian McTighe. {isMobile && <br />}Coded by Hand.
      </Typography>
    </Box>
  );
};

export default Footer;
