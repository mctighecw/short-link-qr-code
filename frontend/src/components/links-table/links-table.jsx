import React from "react";
import useDeleteLink from "@/hooks/use-delete-link";
import useLinks from "@/hooks/use-links";
import { convertDate } from "@/utils/general";
import DeleteIcon from "@mui/icons-material/Delete";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import styles from "./styles";

const LinksTable = () => {
  const { data, isLoading, refetch } = useLinks();

  const deleteLink = useDeleteLink();

  const handleDeleteLink = (id) => {
    deleteLink.mutate(id);
  };

  return (
    <Box>
      {isLoading ? (
        <Box sx={styles.loadingBox}>
          <CircularProgress size={60} thickness={3} />
        </Box>
      ) : (
        <Box>
          <Button variant="outlined" onClick={() => refetch()} sx={styles.button}>
            Refresh
          </Button>
          <TableContainer component={Paper}>
            <Table sx={styles.table} aria-label="links table">
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell>Link Hash</TableCell>
                  <TableCell>Original Link</TableCell>
                  <TableCell>Short Link</TableCell>
                  <TableCell>Count</TableCell>
                  <TableCell>Last Accessed</TableCell>
                  <TableCell>Created At</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(data?.data || []).map((row) => (
                  <TableRow key={row.id} sx={styles.tableRow}>
                    <TableCell>{row.id}</TableCell>
                    <TableCell>{row.linkHash}</TableCell>
                    <TableCell>{row.originalLink}</TableCell>
                    <TableCell>{row.shortLink}</TableCell>
                    <TableCell>{row.count}</TableCell>
                    <TableCell>{convertDate(row.lastAccessedAt)}</TableCell>
                    <TableCell>{convertDate(row.createdAt)}</TableCell>
                    <TableCell>
                      <Box sx={styles.actionsBox}>
                        <DeleteIcon color="error" sx={styles.icon} onClick={() => handleDeleteLink(row.id)} />
                      </Box>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      )}
    </Box>
  );
};

export default LinksTable;
