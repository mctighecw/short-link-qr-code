const styles = {
  loadingBox: {
    display: "flex",
    justifyContent: "center",
    mt: "5vh",
  },
  button: {
    display: "block",
    marginLeft: "auto",
    mb: 2,
  },
  table: {
    minWidth: 650,
  },
  tableRow: {
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  },
  actionsBox: {
    display: "flex",
    justifyContent: "center",
  },
  icon: {
    cursor: "pointer",
  },
};

export default styles;
