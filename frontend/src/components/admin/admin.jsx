import React, { useState } from "react";
import LinksTable from "@/components/links-table";
import LoginBox from "@/components/login-box";
import Navigation from "@/components/navigation";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";

const styles = {
  title: {
    textAlign: "center",
  },
};

const Admin = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  return (
    <>
      <Navigation />
      <Container maxWidth="lg">
        {isAuthenticated ? (
          <Box py={2}>
            <Typography variant="h4" mb={2} sx={styles.title}>
              Admin Page
            </Typography>
            <LinksTable />
          </Box>
        ) : (
          <LoginBox onAuthenticate={() => setIsAuthenticated(true)} />
        )}
      </Container>
    </>
  );
};

export default Admin;
