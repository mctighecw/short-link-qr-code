import React from "react";
import useTheme from "@/hooks/use-theme";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import LightModeIcon from "@mui/icons-material/LightMode";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

const Navigation = () => {
  const { theme, toggleTheme } = useTheme();

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">Short Link and QR Code</Typography>
          <IconButton
            size="large"
            aria-label="theme"
            aria-controls="navigation-menu"
            aria-haspopup="true"
            onClick={toggleTheme}
            color="inherit"
            sx={{ marginLeft: "auto" }}
          >
            {theme === "light" ? <DarkModeIcon /> : <LightModeIcon />}
          </IconButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Navigation;
