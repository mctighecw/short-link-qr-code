const styles = {
  box: {
    boxShadow: 3,
    borderRadius: 2,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    p: 5,
    mt: "15vh",
  },
  fieldBox: { mt: 1 },
  button: { mt: 3, mb: 2 },
  error: { height: 10 },
};

export default styles;
