import React, { useState } from "react";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import styles from "./styles";

const adminAccessKey = process.env.ADMIN_ACCESS_KEY;

const LoginBox = ({ onAuthenticate }) => {
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [loginError, setLoginError] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();

    if (password === adminAccessKey) {
      onAuthenticate();
      return;
    }
    setLoginError("Invalid password");
  };

  return (
    <Container maxWidth="sm">
      <Box component={Paper} sx={styles.box}>
        <Typography component="h1" variant="h5">
          Admin
        </Typography>
        <Box component="form" noValidate onSubmit={handleSubmit} sx={styles.fieldBox}>
          <TextField
            required
            fullWidth
            margin="normal"
            id="password"
            label="Password"
            type={showPassword ? "text" : "password"}
            value={password}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => setShowPassword(!showPassword)}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
            onChange={(event) => setPassword(event.target.value)}
          />
          <Button type="submit" fullWidth variant="contained" disabled={!password} sx={styles.button}>
            Submit
          </Button>
        </Box>

        <Box sx={styles.error}>{loginError && <Typography color="error">{loginError}</Typography>}</Box>
      </Box>
    </Container>
  );
};

export default LoginBox;
