import React, { useState } from "react";
import DisplayData from "@/components/display-data";
import Footer from "@/components/footer";
import Navigation from "@/components/navigation";
import SubmitLink from "@/components/submit-link";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import styles from "./styles";

const Main = () => {
  const [data, setData] = useState(null);

  return (
    <Box sx={styles.box}>
      <Box>
        <Navigation />
        <Container maxWidth="sm">
          <Box pt={2}>
            <Typography variant="h5" mb={1}>
              Link
            </Typography>
            {!data && (
              <Typography variant="body2" mb={3}>
                Enter a URL below and press submit. You will get a short link and a QR code to scan.
              </Typography>
            )}
          </Box>
          <SubmitLink setData={setData} />
          {data && <DisplayData data={data} />}
        </Container>
      </Box>
      <Footer />
    </Box>
  );
};

export default Main;
