import React, { useState } from "react";
import useCreateLink from "@/hooks/use-create-link";
import { validateUrl } from "@/utils/validations";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import styles from "./styles";

const SubmitLink = ({ setData }) => {
  const [url, setUrl] = useState("");
  const [autoFocus, setAutoFocus] = useState(true);
  const [isInvalid, setIsInvalid] = useState(false);

  const createLink = useCreateLink();

  const handleSubmit = () => {
    const isValidUrl = validateUrl(url);
    setIsInvalid(!isValidUrl);

    if (isValidUrl) {
      const payload = {
        original_link: url,
      };
      createLink.mutate(payload, {
        onSuccess: (response) => {
          setData(response?.data);
          setUrl("");

          // Unfocus textfield
          setAutoFocus(false);
          document.activeElement.blur();
        },
      });
    }
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      handleSubmit();
    }
  };

  return (
    <Box display="flex">
      <TextField
        label="Original URL"
        placeholder="www.example.com"
        variant="outlined"
        value={url}
        autoFocus={autoFocus}
        error={isInvalid}
        helperText={isInvalid ? "Invalid URL format" : null}
        onChange={(event) => setUrl(event.target.value)}
        onKeyDown={handleKeyDown}
        sx={styles.textField}
      />
      <Button variant="contained" disabled={url === ""} onClick={handleSubmit} sx={styles.submitButton}>
        Submit
      </Button>
    </Box>
  );
};

export default SubmitLink;
