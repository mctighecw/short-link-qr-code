const styles = {
  textField: {
    width: "100%",
    marginRight: "5px",
  },
  submitButton: {
    maxHeight: 56,
  },
};

export default styles;
