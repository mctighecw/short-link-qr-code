import React from "react";
import QRCode from "react-qr-code";
import Box from "@mui/material/Box";

const styles = {
  box: {
    height: "auto",
    margin: "0 auto",
    maxWidth: 350,
    width: "100%",
    p: 1,
  },
  qrCode: {
    height: "auto",
    maxWidth: "100%",
    width: "100%",
  },
};

const DisplayQRCode = ({ value }) => (
  <Box sx={styles.box}>
    <QRCode size={256} style={styles.qrCode} value={value} viewBox="0 0 256 256" />
  </Box>
);

export default DisplayQRCode;
