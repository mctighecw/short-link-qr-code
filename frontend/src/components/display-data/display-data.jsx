import React from "react";
import DisplayQRCode from "@/components/display-qr-code";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";

const isProduction = process.env.NODE_ENV === "production";

const styles = {
  box: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  card: {
    width: "100%",
    maxWidth: 400,
  },
  link: {
    wordBreak: "break-all",
  },
  extraPadding: {
    px: 3,
  },
  marginBottom: {
    mb: 2,
  },
};

const DisplayData = ({ data }) => {
  const prefix = isProduction ? "https://" : "http://";
  const shortLinkUrl = `${prefix}${data.shortLink}`;

  const handleClickLink = () => {
    window.open(shortLinkUrl, "_blank");
  };

  return (
    <Box py={2} sx={styles.box}>
      <Card raised sx={{ ...styles.card, ...styles.extraPadding, ...styles.marginBottom }}>
        <CardContent>
          <Typography variant="h5" mt={3} mb={1}>
            Original link:
          </Typography>

          <Typography variant="body1" mb={4} sx={styles.link}>
            {data.originalLink}
          </Typography>

          <Typography variant="h5" mb={1}>
            Short link:
          </Typography>

          <Box display="flex" alignContent="center">
            <Button color="primary" size="large" onClick={handleClickLink} sx={{ textTransform: "none" }}>
              {shortLinkUrl}
            </Button>
            <IconButton
              size="large"
              onClick={() => {
                navigator.clipboard.writeText(shortLinkUrl);
              }}
            >
              <ContentCopyIcon />
            </IconButton>
          </Box>
        </CardContent>
      </Card>

      <Card raised sx={styles.card}>
        <DisplayQRCode value={shortLinkUrl} />
      </Card>
    </Box>
  );
};

export default DisplayData;
