import React from "react";
import ReactDOMClient from "react-dom/client";
import { QueryClient, QueryClientProvider } from "react-query";
import { BrowserRouter } from "react-router-dom";
import Routes from "@/components/routes";
import { ThemeProvider } from "@/context/theme";
import CssBaseline from "@mui/material/CssBaseline";
import "@/styles/global.css";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: false,
    },
    mutations: {
      retry: false,
    },
  },
});

const container = document.getElementById("root");
const root = ReactDOMClient.createRoot(container);

root.render(
  <QueryClientProvider client={queryClient}>
    <ThemeProvider>
      <CssBaseline />
      <BrowserRouter>
        <Routes />
      </BrowserRouter>
    </ThemeProvider>
  </QueryClientProvider>
);
