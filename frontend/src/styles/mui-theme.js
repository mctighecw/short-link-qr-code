import { blue, red } from "@mui/material/colors";
import { createTheme } from "@mui/material/styles";

const shared = {
  typography: {
    fontFamily: ["Open Sans", "sans-serif"].join(","),
  },
};

export const lightTheme = createTheme({
  palette: {
    primary: {
      main: blue[700],
    },
    secondary: {
      main: red[700],
    },
  },
  ...shared,
});

export const darkTheme = createTheme({
  palette: {
    mode: "dark",
  },
  ...shared,
});
