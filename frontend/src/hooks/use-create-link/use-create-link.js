import { useMutation } from "react-query";
import request from "@/utils/network";

const createLink = async (payload) => {
  const { data: response } = await request({
    url: "link/create",
    method: "POST",
    data: payload,
  });

  return response;
};

const useCreateLink = () => useMutation((payload) => createLink(payload));

export default useCreateLink;
