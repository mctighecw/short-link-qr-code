import { useQuery } from "react-query";
import request from "@/utils/network";

const getLinks = async () => {
  const headers = { "Admin-Access-Key": process.env.ADMIN_ACCESS_KEY };

  const { data: response } = await request({
    url: "link",
    headers,
  });

  return response;
};

const useLinks = (enabled = true) =>
  useQuery(["links"], getLinks, {
    enabled,
  });

export default useLinks;
