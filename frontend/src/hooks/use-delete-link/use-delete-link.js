import { useMutation, useQueryClient } from "react-query";
import request from "@/utils/network";

const deleteLink = async (id) => {
  const headers = { "Admin-Access-Key": process.env.ADMIN_ACCESS_KEY };

  const { data: response } = await request({
    url: `/link/${id}`,
    method: "DELETE",
    headers,
  });

  return response;
};

const useDeleteLink = () => {
  const queryClient = useQueryClient();

  return useMutation((id) => deleteLink(id), {
    onSuccess: () => {
      queryClient.refetchQueries(["links"]);
    },
  });
};

export default useDeleteLink;
