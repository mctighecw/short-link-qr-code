import React, { createContext, useState } from "react";
import { darkTheme, lightTheme } from "@/styles/mui-theme";
import { checkAndSetTheme, setTheme as setThemeInStorage } from "@/utils/theme";
import { ThemeProvider as MUIThemeProvider } from "@mui/material";

const defaultTheme = checkAndSetTheme();

const ThemeContext = createContext({ theme: defaultTheme, toggleTheme: () => {} });

const ThemeProvider = ({ children }) => {
  const [theme, setTheme] = useState(defaultTheme);
  const muiTheme = theme === "light" ? lightTheme : darkTheme;

  const toggleTheme = () => {
    const value = theme === "light" ? "dark" : "light";

    setThemeInStorage(value);
    setTheme(value);
  };

  return (
    <MUIThemeProvider theme={muiTheme}>
      <ThemeContext.Provider value={{ theme, toggleTheme }}>{children}</ThemeContext.Provider>
    </MUIThemeProvider>
  );
};

export { ThemeContext, ThemeProvider };
