# README

A web app that generates a short URL link and a QR code to scan for the link.

_Note_: Based on my React-Go web server template.

## App Information

App Name: short-link-qr-code

Created: May-July 2024

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/short-link-qr-code)

Production: [Link](https://short-link.mctighecw.site)

## Tech Stack

_Frontend_:

- JavaScript
- React
- React Router
- Webpack
- Material UI
- Prettier
- ESLint

_Backend_:

- Go v.1.20
- Go Modules
- Labstack Echo (v.4) framework
- Zap Logger
- MongoDB
- Swagger
- Docker
- NGINX

## Setup & Start

1. Copy the `.env.example` file to `.env` and then add your custom environmental variables.

2. Start the containers by running:

```sh
$ make dev-up
```

Frontend is running at `http://localhost:3000` and Backend is running at `http://localhost:8000`.

To stop everything:

```sh
$ make dev-down
```

## Frontend Checks

```sh
docker exec -it short-link-qr-code-frontend yarn lint:check
docker exec -it short-link-qr-code-frontend yarn format:check
```

## API Documentation

Documentation for the API is generated by [Swagger for Go](https://github.com/swaggo/swag).

In development, view it at `http://localhost:8000/swagger/index.html`.

Last updated: 2024-10-17
